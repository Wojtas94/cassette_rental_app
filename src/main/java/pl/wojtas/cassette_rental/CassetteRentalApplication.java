package pl.wojtas.cassette_rental;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CassetteRentalApplication {

    public static void main(String[] args) {
        SpringApplication.run(CassetteRentalApplication.class, args);
    }

}
