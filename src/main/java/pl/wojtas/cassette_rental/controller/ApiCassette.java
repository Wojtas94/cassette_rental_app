package pl.wojtas.cassette_rental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import pl.wojtas.cassette_rental.model.Cassette;
import pl.wojtas.cassette_rental.service.CassetteService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class ApiCassette {


    private CassetteService cassetteService;

    @Autowired
    public ApiCassette(CassetteService cassetteService) {
        this.cassetteService = cassetteService;
    }

    @GetMapping("/getAllCassettes")
    public ModelAndView getAllCassettesGet() {
        System.out.println(cassetteService.findAll());
        return new ModelAndView("getAllCassettesView", "cassettes", cassetteService.findAll());
    }

    @PostMapping("/getAllCassettes")
    public RedirectView getAllCassettesPost(HttpServletRequest request, HttpSession session) {
        session.setAttribute("id", Long.valueOf(request.getParameter("id")));
        return cassetteService.choice(request);
    }

    @GetMapping("/addCassette")
    public ModelAndView addCassetteGet() {
        return new ModelAndView("addCassetteView", "cassetteToAdd", new Cassette());
    }

    @PostMapping("/addCassette")
    public RedirectView addCassettePost(@ModelAttribute("cassetteToAdd") Cassette cassette) {
        System.out.println(cassette);
        cassetteService.add(cassette);
        return new RedirectView("getAllCassettes");
    }

    @GetMapping("/editCassette")
    public ModelAndView editCassetteGet(HttpSession session) {
        Long id = (Long) session.getAttribute("id");
        Cassette cassette = cassetteService.getById(id);
        return new ModelAndView("editCassetteView", "cassetteToEdit", cassette);
    }

    @PostMapping("/editCassette")
    public RedirectView editCassettePost(@ModelAttribute Cassette cassette, HttpSession session) {
        cassetteService.edit(cassette, session);
        return new RedirectView("getAllCassettes");
    }

    @GetMapping("/deleteCassette")
    public RedirectView deleteCassetteGet(HttpSession session) {
        Long id = (Long) session.getAttribute("id");
        cassetteService.remove(id);
        return new RedirectView("getAllCassettes");
    }
}
