package pl.wojtas.cassette_rental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import pl.wojtas.cassette_rental.model.User;
import pl.wojtas.cassette_rental.service.UserService;

@Controller
public class ApiUser {

    private UserService userService;

    @Autowired
    public ApiUser(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/createNewUser")
    public ModelAndView createNewUserGet() {
        return new ModelAndView("createNewUserView", "newUser", new User());
    }

    @PostMapping("/createNewUser")
    public RedirectView createNewUserPost(@ModelAttribute User user) {
        userService.add(user);
        return new RedirectView("getAllCassettes");
    }
}
