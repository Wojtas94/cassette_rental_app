package pl.wojtas.cassette_rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.wojtas.cassette_rental.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
