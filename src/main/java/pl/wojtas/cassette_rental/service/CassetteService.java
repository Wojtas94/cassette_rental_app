package pl.wojtas.cassette_rental.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.RedirectView;
import pl.wojtas.cassette_rental.model.Cassette;
import pl.wojtas.cassette_rental.repository.CassetteRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class CassetteService {

    private CassetteRepository cassetteRepository;

    @Autowired
    public CassetteService(CassetteRepository cassetteRepository) {
        this.cassetteRepository = cassetteRepository;
    }

    public List<Cassette> findAll() {
        return cassetteRepository.findAll();
    }

    public RedirectView choice(HttpServletRequest request) {
        String choice = request.getParameter("action");
        String page = "";
        switch (choice) {
            case "ADD" :
                page = "/addCassette";
                break;
            case "EDIT" :
                page = "/editCassette";
                break;
            case "DELETE" :
                page = "/deleteCassette";
                break;
            case "NEW_USER" :
                page = "/createNewUser";
                break;
        }
        return new RedirectView(page);
    }

    public void add(Cassette cassette) {
         cassetteRepository.save(cassette);
    }

    public Cassette getById(Long id) {
        return cassetteRepository.getOne(id);
    }

    public void edit(Cassette cassette, HttpSession session) {
        cassette.setId((Long) session.getAttribute("id"));
        System.out.println(cassette.getId());
        cassetteRepository.save(cassette);
    }

    public void remove(Long id) {
        cassetteRepository.delete(cassetteRepository.getOne(id));
    }
}
