insert into cassettes.cassette (title, release_date, author) values ('Titanic', '1966-04-04', 'James Cameron');
insert into cassettes.cassette (title, release_date, author) values ('Joker', '2019-11-09', 'Todd Philips');
insert into cassettes.cassette (title, release_date, author) values ('Fast and furious', '2011-07-03', 'David Leitch');